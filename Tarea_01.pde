Cadena str;
Boton b1,b2,b3,b4,b5,b6,b7;
PFont p;
String men;
public void setup(){
    size(520,400);
    smooth();
    str = new Cadena("Tarea_01");
    p = createFont("Arial",20);
    textFont(p);
    b1 = new Boton(20,25,"BorraInicio");
    b2 = new Boton(200,25,"AgregaFinal");
    b3 = new Boton(380,25,"Vacía");
    b4 = new Boton(20,75,"Llena");
    b5 = new Boton(200,75,"Invertir");
    b6 = new Boton(380,75,"BorraFinal");
    b7 = new Boton(20,125,"AgregaInicio");
    men ="Aqui va el mensaje";
}

public void draw(){
     background(255);
     fill(0,128,192);
     text(str.toString(),60,250);
     b1.dibujar();
     b2.dibujar();
     b3.dibujar();
     b4.dibujar();
     b5.dibujar();
     b6.dibujar();
     b7.dibujar();
     fill(64,0,64);
     text(men,20,height-30);
}

public void mousePressed(){
    men = b1.click(mouseX, mouseY,men);
    men = b2.click(mouseX, mouseY,men);
    men = b3.click(mouseX, mouseY,men);
    men = b4.click(mouseX, mouseY,men);
    men = b5.click(mouseX, mouseY,men);
    men = b6.click(mouseX, mouseY,men);
    men = b7.click(mouseX, mouseY,men);
   
}