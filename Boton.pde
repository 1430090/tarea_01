public class Boton{
    float x;
    float y;
    String mensaje;
    float ancho=120;
    float alto=30;
    
    public Boton(float a, float b, String c){
        x=a;
        y=b;
        mensaje = c;
    }
    
    public void dibujar(){
        fill(51,0,0);
        rect(x,y,ancho,alto);
        fill(255,0,128);
        text(mensaje,x+5,y+20);
    }
    
    public boolean isAdentro(float a, float b){
         return ( a >= x && a <=  ( x+ancho ) && b >= y  && b <= (y+alto)   );
    }
  
    
    public String click(float a, float q,String men){
        boolean ban = isAdentro(a,q);
        if(ban)
        return mensaje;
        else
        return men;
        
        
    }
    
  
    
  

}